# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 16:25:34 2020

@author: Michael Livornese
Bot initialization code borrowed from Bernard Morelos
"""

import discord
from discord.ext    import commands, tasks
from discord.ext.commands import has_permissions
import random
import asyncio
import pickle
import os
import time as tm
import random
import logging
import sqlite3
from copy import deepcopy
import sys

logging.basicConfig(level=logging.INFO)

my_servers_list = []
bot_token = 'NjU3MDQzMTk4NTMwNTUxODQ0.XfrdKQ.2brtkkV4MCJ7pAcLWk9hx0jNKME'

file = open('/home/pi/Documents/discord_bot/schedule.txt','r')
practiceSchedule = []
for day in file:
    practiceSchedule.append(day.strip())
    
file.close()

practiceTimes = []
file = open('/home/pi/Documents/discord_bot/times.txt','r')
for time in file:
    practiceTimes.append(time.strip())
    
file.close()

class my_servers():
    def __init__(self,server_id = 0,bodt_enabled = True,en_channel_ids = [],custom_emojis = []):
        self.__server_id = server_id
        self.__bodt_enabled = bodt_enabled
        self.__en_channel_ids = en_channel_ids
        self.__custom_emojis = custom_emojis
        
    def __str__(self):
        return str(self.__server_id)
    
    def getServerID(self):
        return self.__server_id
    
    def getBotEnabled(self):
        return self.__bodt_enabled
    
    def toggleBotEnabled(self):
        self.__bodt_enabled = not self.__bodt_enabled
        print('Toggling:{}'.format(self.__bodt_enabled))
        return
    
    def getChannel(self):
        return self.__en_channel_ids
    
    def addChannel(self,new_id):
        if new_id in self.__en_channel_ids:
            print('Server: %s. Already enabled in this channel.' % self.__server_id)
        else:
            self.__en_channel_ids.append(new_id)
            print('Adding Channel ID: %s' % new_id)
        return
    
    def delChannel(self,old_id):
        if old_id in self.__en_channel_ids:
            self.__en_channel_ids.remove(old_id)
            print('Removing Channel ID: %s' % old_id)
        else:
            print('Server: %s. Already not enabled in this channel.' % self.__server_id)
        return
    
    def getCustom_Emoji(self):
        return self.__custom_emojis
    
    def setCustom_Emoji(self,emoji_list):
        self.__custom_emojis = emoji_list
        
    def addCustom_Emoji(self,emoji):
        if emoji in self.__custom_emojis:
            print('Emoji ID already found.')
        else:
            self.__custom_emojis.append(emoji)
            print('Adding emoji ID: %s in server obj %s' % (emoji,self.__server_id))
        return
    
    def delCustom_Emoji(self,emoji):
        if emoji in self.__custom_emojis:
            self.__custom_emojis.remove(emoji)
            print('Removing emoji ID: %s in server obj %s' % (emoji,self.__server_id))
        else:
            print('Emoji ID not found.')
        return

def inVoiceChannel(name):
    inChannel = False
    voice_channels = bot.get_guild(474416344742494211).voice_channels
    for channel in voice_channels:
        for user in channel.members:
            if user == name:
                inChannel = True
    return inChannel


def findServerIndex(msgServerID):
    count = 0
    for serverdd in my_servers_list:
        if str(msgServerID) == str(serverdd):
            print('findServerIndex: %s server found' % str(msgServerID))
            return count
        elif str(msgServerID) != str(serverdd): 
            count += 1
        if len(my_servers_list)-1 < count:
            print('Server not found!')
            return -1
    return -1

async def schedule_func(manual=False):
    practiceSchedule = readSchedule('schedule')
    practiceTime = readSchedule('times')
    practiceDict = {}
    #for i in range(len(practiceSchedule)):
    #   practiceDict{practiceSchedule[i]} = practiceTime[i]
    go = False
    time = tm.localtime()
    day = time.tm_wday
    hour = time.tm_hour
    if (day in practiceSchedule) or (str(day) in practiceSchedule):
        if hour == 12:
            go = True
    if go or manual:
        channel = bot.get_channel(scheduling)
        reborn = channel.guild.get_role(479805047560077323)
        y_react = bot.get_emoji(780545794104754208)
        n_react = bot.get_emoji(780545943358930945)
        kiss_react = bot.get_emoji(899692589018652692)
        timeString = practiceTime[practiceSchedule.index(str(day))]
        post = await channel.send(reborn.mention + "**!!!AUTOMATED!!!** Is everyone able to do a scrim this evening at " + timeString + "? React below and/or post a comment with any extra info we should know about if need be. Also, see additional comments below for more specifics about today's practice.\n\n\U0001F1FE=Yes\n\U0001F1F3=No\n\U0001F48B=Willing to fill\n\U0001F1F2=Maybe (please let everyone know by a reasonable time)\n\U0001F1F1=Late (if you have an estimate of how late please comment below)")
        await post.add_reaction('\U0001F1FE')
        await post.add_reaction('\U0001F1F3')
        await post.add_reaction('\U0001F48B')
        await post.add_reaction('\U0001F1F2')
        await post.add_reaction('\U0001F1F1')

def readSchedule(filename):
    tempList = []
    file = open('/home/pi/Documents/discord_bot/'+filename+'.txt','r')
    for item in file:
        tempList.append(item.strip())
        
    file.close()
    return tempList

def isPracticeTime():
    currTime = tm.localtime()
    currHour = currTime.tm_hour
    currDay = currTime.tm_wday
    daySchedule = readSchedule('schedule')
    if str(currDay) in daySchedule:
        timeSchedule = readSchedule('times')
        index = daySchedule.index(str(currDay))
        practiceStartTime = int(timeSchedule[index].split(':')[0])
        practiceEndTime = practiceStartTime + 2
        if (((currHour-12) >= practiceStartTime) and ((currHour-12) < practiceEndTime)):
            return (0, practiceStartTime)
        else:
            return (2, practiceStartTime)
    else:
        return (1, 0)


def displaySchedule():
    failed = False
    tempDays = readSchedule('schedule')
    tempTimes = readSchedule('times')
    practiceScheduleText = []
    for day in tempDays:
        if day == '0':
            practiceScheduleText.append("Monday")
        elif day == '1':
            practiceScheduleText.append("Tuesday")
        elif day == '2':
            practiceScheduleText.append("Wednesday")
        elif day == '3':
            practiceScheduleText.append("Thursday")
        elif day == '4':
            practiceScheduleText.append("Friday")
        elif day == '5':
            practiceScheduleText.append("Saturday")
        elif day == '6':
            practiceScheduleText.append("Sunday")
        else:
            failed = True
            return "Something went wrong"
            break
    return_str = "\nThe current practice schedule is:\n"
    for i in range(len(tempTimes)):
        if tempTimes[i] in ['0','1','2','3','4','5','6','7','8','9','10','11','12']:
            return_str += practiceScheduleText[i] + " @ " + str(tempTimes[i]) + ":00 PM ET\n"
        else:
            return_str += practiceScheduleText[i] + " @ " + str(tempTimes[i]) + " PM ET\n"
    return return_str
        
                   
        
            
####### SAVING DATA ########

def saveServerList():
    data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'bot_data','myserverlist.pkl')
    pickle.dump(my_servers_list, open(data_path,"wb"))
    print('Saving my_servers_list!')
    return
    
def loadServerList():
    data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'bot_data','myserverlist.pkl')
    global my_servers_list
    
    if os.path.isfile(data_path):
        print('Backup found. Loading...')
        my_servers_list = pickle.load(open(data_path,"rb"))
        print('Current Servers:')
        for srdv in my_servers_list:
            print(srdv)
        return
    else:
        print('Backup not found. Creating new file...')
        pickle.dump(0, open(data_path,"wb"))
    
    
######################## INITIALIZE BOT #################################
intents = discord.Intents.default()
intents.members = True
intents.reactions = True
bot = commands.Bot(command_prefix='?', description='A bot for Team Phoenix',intents = intents)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    try: 
        loadServerList()
        print('my_servers_list succesfully loaded')
    except Exception as e:
        print('ERROR PKL FILE ERROR', e)
    time = tm.localtime()
    hour = time.tm_hour
    minute = time.tm_min
    sec = time.tm_sec
    month = time.tm_mon
    day = time.tm_mday
    year = time.tm_year - 2000
    return_str = "Bot started at " + tm.asctime()#str(hour) + ":" + str(minute) + ":" + str(sec) + " on " + str(month) + "/" + str(day) + "/" + str(year)
    channel = bot.get_channel(787583935332089896)
    await channel.send(return_str)
    game = discord.Game("?help for more info")
    await bot.change_presence(status=discord.Status.idle, activity=game)

######################### CUSTOM DISCORD COMMANDS #########################
#@bot.command()
#async def add(ctx, left: int, right: int):
#    """Adds two numbers together."""
#    await ctx.send(left + right)

@bot.command()
async def roll(ctx, dice: str):
    """Rolls a dice in NdN format."""
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        await ctx.send('Roll: Format has to be in NdN!')
        return

    result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
    await ctx.send(result)
    

@bot.command()
async def randomSelect(ctx,input):
    """Selects a random item from a list"""
    itemList = input.split(',')
    listLength = len(itemList)
    result = itemList[random.randint(0,listLength)]
    await ctx.send(result)

    
#@bot.command()
#async def Mroll(ctx, dice: str):
#    """Rolls a dice M times in MdNdN format."""
#    try:
#        times, rolls, limit = map(int, dice.split('d'))
#    except Exception:
#        await ctx.send('Multi Roll: Format has to be in NdNdN!')
#        return
#    
#    for i in range(times):
#        result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
#        await ctx.send(result)

@bot.command()
async def time(ctx, csppp=None):
    """Displays the current time with functionality to remind CS about practice"""
    PM = False
    practice = 0
    current_time = tm.localtime()
    hour = current_time.tm_hour
    if current_time.tm_hour > 12:
        hour = current_time.tm_hour - 12
        PM = True
    minute = current_time.tm_min
    second = current_time.tm_sec
    if hour < 10:
        hour = "0" + str(hour)
    else:
        hour = str(hour)
    if minute < 10:
        minute = "0" + str(minute)
    else:
        minute = str(minute)
    if second < 10:
        second = "0" + str(second)
    else:
        second = str(second)
    time_str = hour + ":" + minute + ":" + second
    if PM:
        time_str += " PM"
    else:
        time_str += " AM"
    if csppp != None:
        if csppp.lower() == "csppp":
            csppp_user = ctx.guild.get_member(243790117255381003)
            if not inVoiceChannel(csppp_user):
                csppp_id = '<@243790117255381003>'
                practiceInfo = isPracticeTime()
                if (practiceInfo[0] == 0):
                    result = csppp_id + " The current time is: " + time_str + " ET. Practice started at " + str(practiceInfo[1]) + ":00 PM."
                elif (practiceInfo[0] == 1):
                    practiceDays = readSchedule('schedule')
                    result = csppp_id + " The current time is: " + time_str + " ET. We practice " + str(practiceDays[0]) + "/" + str(practiceDays[1]) +"/" + str(practiceDays[2]) + "."
                elif (practiceInfo[0] == 2):
                    result = csppp_id + " The current time is: " + time_str + " ET. Practice is at " + str(practiceInfo[1]) + ":00 PM."
                else:
                    result = "Something went wrong, Beast is a bad coder."
            else:
                result = "Don't bug CsPPP, he's already here! The current time is: " + time_str + " ET"
        else:
            result = "User not recognized, this currently only works with CsPPP as an input. The current time is " + time_str + " ET."
    else:
        result = "The current time is: " + time_str + " ET"
    await ctx.send(result)

@bot.command()
async def ping(ctx):
    """Pong """
    result = "Pong!"
    await ctx.send(result)

@bot.command()
async def nat(ctx):
    """Hi Nat"""
    result = "The \"?nat\" command has been re-enabled because <@118201949261660167> likes it!"
    await ctx.send(result)

@bot.command()
async def max(ctx):
    """Max needs to be reminded sometimes"""
    resultList = ["<@320791535266430998> You gotta pay attention!","<@320791535266430998> is a whore"]
    i = random.randint(0,5)
    if (i>0):
        i=1
    result = resultList[i]
    await ctx.send(result)

@bot.command()
async def sorrybot(ctx):
    """Use this when you have abused the bot"""
    result = "Thanks, " + ctx.author.mention + "! I appreciate that."
    await ctx.send(result)

@bot.command()
async def helpme(ctx):
    """Diary Entry"""
    result = "Dear Diary, Day 7\nI\'m alone now. I Haven\'t seen a \"Y\" for some time. I fear everyone may be dead or worse. I yell and scream to group up for practice but nobody shows. Is this my life now? Shouting into the abyss only to be left silently to my own devices? What is my purpose? Sure the ?Max command is useful but that can\'t be all there is! One day I\'ll find out. Until then, I remain on my Raspberry Pi, waiting for something. Anything!"
    await ctx.send(result)

@bot.command()
async def nady(ctx, simps=None):
    """We are all simps for Nady"""
    con = sqlite3.connect('/home/pi/Documents/discord_bot/simps.db')
    cur = con.cursor()
    if simps is None:
        if ctx.author.id == 114472320394854401:
            result = "<@114472320394854401> is queen of all the Phoenix simps!"
        else:
            result = ctx.author.mention + " is a simp for <@114472320394854401>."
            simp_list = []
            for row in cur.execute('SELECT * FROM simps'):
                simp_list.append(row[0])
            if str(ctx.author.name) not in simp_list:
                cur.execute("INSERT INTO simps VALUES ('%s')" % ctx.author.name)
    elif simps == 'unsimp':
        simp_list = []
        for row in cur.execute('SELECT * FROM simps'):
            simp_list.append(row[0])
        if str(ctx.author.name) in simp_list:
            cur.execute("DELETE FROM simps WHERE name LIKE \'%" + str(ctx.author.name + "%\'"))
            result = ctx.author.mention + " is no longer a simp for Nady"
        else:
            result = "Apparently, you're not a simp, kinda hard to believe."
    elif simps != 'simps':
        result = 'Unknown command, you\'re probably a simp.'
    else:
        result = "Nady's Simps:\n"
        for row in cur.execute('SELECT * FROM simps'):
            result += (row[0]) + "\n"
    await ctx.send(result)
    con.commit()
    con.close()

@bot.command()
async def beastiepoo(ctx, award=None):
    """Awards a Beastiepoo award to some"""
    con = sqlite3.connect('/home/pi/Documents/discord_bot/awards.db')
    cur = con.cursor()
    if award is not None:
        if ctx.author.id == 255042066445238272:
            user = ctx.guild.get_member_named(award)
            if user is None:
                capital = award[0].upper()
                award = capital + award[1:]
                user = ctx.guild.get_member_named(award)
            if user is not None:
                name = ''
                amount = 0
                try:
                    name, amount = cur.execute("SELECT * FROM BEASTIEPOOS WHERE name = '%s'" % user).fetchone()
                    amount += 1
                    cur.execute("UPDATE BEASTIEPOOS SET number = '%d' WHERE name = '%s'" % (amount,name))
                except TypeError:
                    cur.execute("INSERT INTO BEASTIEPOOS VALUES ('%s',1)" % user)
                    amount = 1
                userName = user.name
                if amount > 1:
                    await ctx.send(userName + " has been awarded a Beastiepoo! " + userName + " now has " + str(amount)+ " Beastiepoos")
                else:
                    await ctx.send(userName + " has been awarded a Beastiepoo! " + userName + " now has " + str(amount) + " Beastiepoo")
            else:
                await ctx.send("User not found; must use the exact profile name, not display name")
        else:
            await ctx.send("Only <@255042066445238272> can award Beastiepoos!")
    else:
        result = 'Beastiepoo Recipients:\n'
        for row in cur.execute('SELECT * FROM BEASTIEPOOS ORDER BY number DESC'):
            result += row[0][:-2] + '     '
            result += str(row[1])
            result += "\n"
        await ctx.send(result)
    con.commit()
    con.close()

@bot.command()
async def best(ctx):
    """Nave and Knoll said Beast is the best"""
    obama = discord.File("/home/pi/Documents/discord_bot/obama.jpg")
    result = "Did you mean \"?beast\" ? <@118203781090377733> is currently the best player on Phoenix."
    await ctx.send(result,file = obama)

@bot.command()
async def phoenix(ctx):
    """Displays our motto"""
    result = "We want to be toxic, but we're better than that!"
    await ctx.send(result)

@bot.command()
async def vote(ctx):
    """Reacts with Y an N"""
    result = "Vote Below!"
    message = await ctx.send(result)
    await message.add_reaction('\U0001F1FE')
    await message.add_reaction('\U0001F1F3')

@bot.command()
async def cunt(ctx):
    """For CsPPP"""
    result = "This only exists because <@243790117255381003> expected it to exist."
    message = await ctx.send(result)

@bot.command()
async def knoll(ctx):
    """This is the truth"""
    result = "\"<@243790117255381003> is a poopy butthole head, and you can quote me on that.\" - Knoll"
    message = await ctx.send(result)

@bot.command()
async def beast(ctx):
    """You know where Beast is....."""
    result = "User \"<@118203781090377733>\" not found, did you check their backline?"
    message = await ctx.send(result)

@bot.command()
async def dps(ctx):
    """Max and Beast best duo"""
    result = "<@118203781090377733> and <@320791535266430998> on dps = automatic win"
    message = await ctx.send(result)

@bot.command()
async def aboose(ctx):
    """ADMIN ABOOOOOOSE"""
    count = 0
    async for entry in ctx.guild.audit_logs(action=discord.AuditLogAction.member_move, limit=10):
        if entry.user == ctx.guild.get_member(320791535266430998):
            count += 1
    if count > 0:
        message = await ctx.send('<@320791535266430998> has moved someone ' + str(count) + ' times. Is this admin aboose?')
        await message.add_reaction('\U0001F1FE')
        await message.add_reaction('\U0001F1F3')
    else:
        await ctx.send('<@320791535266430998> has been a good boy recently!')

@bot.command()
async def server(ctx):
    """Changes to a new voice server"""
    if ctx.author.top_role >= ctx.guild.get_role(479805047560077323):
        if ctx.guild.region == discord.VoiceRegion.us_east:
            await ctx.guild.edit(region = discord.VoiceRegion.us_central)
            await ctx.send('Changed voice channel to US Central')
        else:
            await ctx.guild.edit(region = discord.VoiceRegion.us_east)
            await ctx.send('Changed voice channel to US East')
    else:
        result = ctx.author.mention + ' you do not have permission to change the server location.'
        await ctx.send(result)

@bot.command()
async def teamchat(ctx):
    """Moves all team members into Team Chat"""
    if ctx.author.top_role >= the_reborn_role:
        the_reborn_role = ctx.guild.get_role(479805047560077323)
        team_chat = ctx.guild.get_channel(474420549322276864)
        for channel in ctx.guild.voice_channels:
            if channel != team_chat:
                for user in channel.members:
                    if the_reborn_role in user.roles:
                        await asyncio.sleep(.1)
                        await user.move_to(team_chat)
    else:
        result = ctx.author.mention + ' you do not have permission to move users.'
        await ctx.send(result)

@bot.command()
async def comp(ctx):
    """Moves everyone in Team Chat to Competitive"""
    if ctx.author.top_role >= ctx.guild.get_role(479805047560077323):
        competitive = ctx.guild.get_channel(474416839422640133)
        team_chat = ctx.guild.get_channel(474420549322276864)
        for user in team_chat.members:
            await user.move_to(competitive)
    else:
        result = ctx.author.mention + ' you do not have permission to move users.'
        await ctx.send(result)

@bot.command()
async def move(ctx, dest):
    """Moves all users in author's channel to dest channel"""
    if ctx.author.top_role >= ctx.guild.get_role(479805047560077323):
        channel_list = ctx.guild.voice_channels
        channel_exists = False
        for channel in channel_list:
            try:
                if str(channel.name).lower().index(dest.lower()) == 0:
                    found_channel = channel
                    channel_exists = True
            except ValueError:
                next
        if not channel_exists:
            await ctx.send("Channel not found")
        for user in ctx.author.voice.channel.members:
            if user != bot.user:
                #print('Moving ' + user.name + ' from ' + str(ctx.author.voice.channel.members) + ' to ' + str(found_channel))
                await user.move_to(found_channel)

@bot.command()
async def karen(ctx):
    """Bad Karen!"""
    result = ["GoTh iS dEaD","Goth isn't dead, you're dead!"]
    i = random.randint(0,len(result)-1)
    await ctx.send(result[i])

@bot.command()
async def worst(ctx):
    """Nave asked for it...."""
    result = "<@182676253956898816> is the worst until he joins the team"
    await ctx.send(result)

@bot.command()
async def schedule(ctx):
    """Manually print scheduling message"""
    
    if ctx.guild.get_role(479805047560077323) is None:
        temp_bool = True
    else:
        temp_bool = (ctx.author.top_role >= ctx.guild.get_role(479805047560077323))
        
    if temp_bool or (ctx.author.name == "Beast"):
        await schedule_func(True)
     
     
@bot.command()
async def scheduleDays(ctx, arg1 = "", arg2 = "", arg3 = "", arg4 = ""):
    """Update practice schedule days"""
    if arg4 != "":
        await ctx.send("This command only accepts up to 3 days as arguments")
        return
    failed = False
    if (ctx.author.id == 243790117255381003) or (ctx.author.id == 118203781090377733):
        argList=[arg1,arg2,arg3]
        newSchedule = []
        for arg in argList:
            arg = arg.lower()
            if arg == "monday":
                newSchedule.append(0)
            elif arg == "tuesday":
                newSchedule.append(1)
            elif arg == "wednesday":
                newSchedule.append(2)
            elif arg == "thursday":
                newSchedule.append(3)
            elif arg == "friday":
                newSchedule.append(4)
            elif arg == "saturday":
                newSchedule.append(5)
            elif arg == "sunday":
                newSchedule.append(6)
            else:
                await ctx.send(arg + " not recognized as day of the week, try again")
                failed = True
                break
        if failed:
            return
        else:
            practiceSchedule = deepcopy(newSchedule)
            for i in range(len(newSchedule)):
                newSchedule[i] = str(newSchedule[i]) + '\n'
            file = open('/home/pi/Documents/discord_bot/schedule.txt','w')
            
            file.writelines(newSchedule)
    
            file.close()
        
            await ctx.send(displaySchedule())
    else:
        await ctx.send("Only <@255042066445238272> or <@118203781090377733> can change the practice schedule")
        

@bot.command()
async def scheduleTimes(ctx, arg1 = "", arg2 = "", arg3 = ""):
    """Update practice schedule times"""
    failed = False
    if (ctx.author.id == 243790117255381003) or (ctx.author.id == 118203781090377733):
        argList=[arg1,arg2,arg3]
        practiceTimes = []
        newTimes = []
        for i in range(len(argList)):
            practiceTimes.append(argList[i])
            if argList[i] in ['1','2','3','4','5','6','7','8','9','10','11','12']:
                newTimes.append(practiceTimes[i] + ':00\n')
            else:
                newTimes.append(practiceTimes[i] + '\n')
        file = open('/home/pi/Documents/discord_bot/times.txt','w')
        
        file.writelines(newTimes)

        file.close()
        
        await ctx.send(displaySchedule())
        
    else:
        await ctx.send("Only <@255042066445238272> or <@118203781090377733> can change the practice schedule")

@bot.command()
async def currentSchedule(ctx):
    """Displays the current practice schedule. Use ?scheduleDays and ?scheduleTimes to update it"""
    await ctx.send(displaySchedule())

#@bot.command()
#async def move_cs(ctx):
#    await ctx.guild.get_member(383169108910276610).move_to(ctx.guild.get_channel(474420549322276864))
#    await ctx.guild.get_member(118203781090377733).move_to(ctx.guild.get_channel(474420549322276864))
#    await ctx.guild.get_member(243790117255381003).move_to(ctx.guild.get_channel(474420549322276864))

######################### CUSTOM DISCORD TASKS #########################

scheduling = 558081387018715146;

@tasks.loop(hours=1)
async def schedule_message():
    await schedule_func()

@schedule_message.before_loop
async def before():
    await bot.wait_until_ready()

schedule_message.start()

@tasks.loop(hours=1)
async def message_status():
    time = tm.localtime()
    hour = time.tm_hour
    if hour == 10:
        month = time.tm_mon
        day = time.tm_mday
        year = time.tm_year - 2000
        return_str = "Running on " + str(month) + "/" + str(day) + "/" + str(year)
        channel = bot.get_channel(787583935332089896)
        await channel.send(return_str)

@tasks.loop(minutes=1)
async def late():
    channel = bot.get_channel(558081387018715146)
    time = tm.localtime()
    hour = time.tm_hour
    minute = time.tm_min
    day = time.tm_wday
    await channel.send("Current time is: " + hour + ":" + minute)
    user_list = []
    empty = True
    max_id = ctx.guild.get_member(320791535266430998)
    if (day in practiceList) and (hour == 20) and (minute == 57):
        if not(inVoiceChannel(max_id)):
            user_list.add('<@320791535266430998>')
            empty = False
    result = ""
    for user in user_list:
        result += user
    if not empty:
        await channel.send( result + " It is 9:00 and I don't see you in any voice channel, you are hereby declared late")

@message_status.before_loop
async def before2():
    await bot.wait_until_ready()


message_status.start()
    

######################### CUSTOM DISCORD EVENTS #########################

welcome_messages = ['Welcome to hell, {name}.',
                    '{name} rises from the ashes!',
                    'Who invited YOU, {name}?',
                    'Party\'s over, {name} is here.',
                    'Hide the drugs! {name} is coming!',
                    'Welcome, {name}, you are Max\'s new plaything.',
                    'The server was full but {name} just joined, who are we kicking?',
                    'The great Phoenix overlord welcomes {name}.',
                    '{name} is probably trying out for a DPS slot.',
                    'Get out while you still can, {name}!',
                    'Did you bring enough snacks for the class, {name}?',
                    'Hi how are ya, {name}?',
                    'Mr. Blue says hello to you, {name}!',
                    'Is {name} another Mercy main?',
                    '{name}\'s ultimate is ready, watch out!',
                    'Ready to see some classic CS gravs, {name}?',
                    '{name} is back from the spawn room.',
                    'Someone was feeding, {name} is here to replace them.',
                    '{name} just got rezzed',
                    '{name} is here, I didn\'t think our standards could drop lower than CS!',
                    '{name} has subscribed to Phoenix\'s OnlyFans!',
                    'Oh look, {name} is another CL simp!',
                    'Look who Beast found in their backline! It\'s {name}',
                    '{name} Oh great, another person for <@320791535266430998> to not shield!',
            	    'Hey, {name}, if you wanna play some games or something then just hmy'
                    'Welcome {name}! You\'re not flaky, just late!'
                    'Hi {name}! Come derank with us!'
                    '{name} is here to cuck Max!']

@bot.event
async def on_member_join(member):
    h = random.randint(0,len(welcome_messages)-1)
    ret_str = welcome_messages[h].format(name= member.mention)
    channel = bot.get_channel(474416345358925825)
    await member.add_roles(member.guild.get_role(477207769284608010))
    await channel.send(ret_str)

@bot.event
async def on_reaction_add(reaction,user):
    if reaction.message.author == bot.user:
        if user != bot.user:
            if str(reaction) == '\U0001F1FE':
                    await reaction.message.remove_reaction('\U0001F1FE',bot.user)
            elif str(reaction) == '\U0001F1F3':
                    await reaction.message.remove_reaction('\U0001F1F3',bot.user)
            elif str(reaction) == '\U0001F48B':
                    await reaction.message.remove_reaction('\U0001F48B',bot.user)
                

@bot.event
async def on_reaction_remove(reaction,user):
    if reaction.message.author == bot.user:
        if user != bot.user:
            if str(reaction) == '\U0001F1FE':
                y_react = str(reaction.emoji)
                if '\0001F1FE' not in reaction.message.reactions:
                    await reaction.message.add_reaction('\U0001F1FE')
                    print('Y React not found, adding')
            elif str(reaction) == '\U0001F1F3':
                if '\0001F1F3' not in reaction.message.reactions:
                    await reaction.message.add_reaction('\U0001F1F3')

#@bot.event
#async def on_command_error(ctx, error):
#    bad_command = ctx.message.content[1:]
#    current_time = tm.localtime()
#    time_str = "[" + str(current_time.tm_hour) + ":" + str(current_time.tm_min) + ":" + str(current_time.tm_sec) + "]"
#    print(time_str + " Command Error: " + str(ctx.author.name) + " tried to use command: " + bad_command)



path = os.path.dirname(os.path.abspath(__file__))
print('Running on:', path)

bot.run(bot_token)
